site_name: IoT Lab Manual
repo_name: 'IoT Lab Manual Git Repo'
repo_url: 'https://gitlab.com/abhas/iot-lab-manual'
pages:
    - Home: index.md
    - About: about.md
    - Building the Documentation: building.md
    - Conventions in this manual: conventions.md
    - Overview of the IoT Kit: overview.md
    - Safeguards and Basics: safeguards.md
    - Getting Started: getting_started.md
    - Common components:
      - Breadboards: common_components/breadboards.md
      - Jumper Wires: common_components/jumper_wires.md
      - Soldering Irons: common_components/soldering_irons.md
      - Wire Strippers: common_components/wire_strippers.md
      - Multimeter: common_components/multimeter.md
    - Power Supplies:
      - Breadboard Power Supply: power_supplies/breadboard.md
      - USB Power Supply: power_supplies/usb.md
      - 12V Power Supply: power_supplies/12v.md
      - Power Strips: power_supplies/power_strip.md
      - LiPo Batteries: power_supplies/lipo.md
    - Sensors:
      - Pulse Sensor: sensors/pulse.md
      - Water Sensor: sensors/water_sensor.md
      - Current Sensor: sensors/current.md
      - Non-Invasive Current Sensor: sensors/current_noninvasive.md
      - Temperature (Analog): sensors/analog_temperature.md
      - Temperature (Digital): sensors/digital_temperature.md
      - Humidity: sensors/humidity.md
      - Simple LED: sensors/led.md
      - RGB LED: sensors/rgb_led.md
      - Switch: sensors/switch.md
      - Hall Magnetic: sensors/hall_effect.md
      - Reed Relay: sensors/reed.md
      - Relay: sensors/relay.md
      - Photo Resistor: sensors/photo_resistor.md
      - Joystick: sensors/joystick.md
    - Microcontrollers:
      - Arduino Mega: microcontrollers/arduino_mega.md
      - Arduino Pro Micro: microcontrollers/arduino_pro_micro.md
      - Digispark: microcontrollers/digispark.md
      - NodeMCU: microcontrollers/nodemcu.md
      - Wemos D1 Mini: microcontrollers/wemos_d1.md
    - Single Board Computers:
      - Raspberry Pi 3: single_board_computers/raspberry_pi_3.md
      - Raspberry Pi Zero: single_board_computers/raspberry_pi_zero.md
      - Onion Omega2: single_board_computers/onion_omega2.md
      - Orange Pi 2G IOT: single_board_computers/orange_pi_2g_iot.md
      - Nano Pi Neo Air: single_board_computers/nano_pi_neo_air.md
    - LEDs and Displays:
      - RGB LED Strip: displays/rgb_leds.md
      - OLED Displays: displays/oled.md
      - OrangePi 2G LCDs: displays/orangepi.md
      - 7" HDMI LCDs: displays/lcds.md
    - Specialised Boards:
      - HealthyPi: specialised_boards/healthypi.md
theme: 
  name: 'material'
  logo:
    icon: 'bug report'
  palette:
    primary: 'deep purple'
    accent: 'red'
  favicon: 'images/favicon.png'
  custom_dir: 'theme'
google_analytics:
  - 'UA-XXXXXXXX-X'
  - 'auto'
markdown_extensions:
  - codehilite
  - admonition
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.superfences
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - toc:
      permalink: true

