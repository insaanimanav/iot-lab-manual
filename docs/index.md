# Welcome to the IoT Lab Manual

Welcome to the home of the IoT Lab Manual. This documentation is
published and maintained using [mkdocs](http://mkdocs.org).

The IoT Lab Manual will guide you on the tools, components and
equipment available as a part of the lab and how to use them.

Please do read this manual carefully. It is good to familiarise yourself
with the contents of your IoT kit along with the corresponding
safeguards before you venture out to use it.

