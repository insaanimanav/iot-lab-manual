# About the lab manual

This IoT Lab Manual is being initially authored as a part of the IoT learning
initiative supported by the RV College of Engineering, Bangalore. The goal of
this lab manual is to serve as an excellent starting point for those who are
just starting their journey into the wonder-filled world of electronics,
microcontrollers, single-board computers and so much more.

## Using this manual

The first time you use this manual, it is recommended that you read the manual
in sequence. At the bottom of each page, you will find a **Next** button.
Clicking that will take you to the next page in sequence. It is recommended that
you use this system (or the *Table of Contents* on the left).

Later on, you can just jump to the relevant section that you need to refer to.

## Tools

This lab manual is written in Markdown using a documentation tool called mkdocs.
It is written using the `vim` editor and changes and versions of the
documentation are managed and tracked uing `git` and `Gitlab`. The 
`Material for mkdocs` theme provides excellent aesthetics and usability for this
manual. Of course, all these tools run on GNU/Linux.

## About the Author

The lab manual is authored by Abhas Abhinav.

Abhas is a hacker, an entrepreneur and a ardent maker. He firmly believes that
the principles of software freedom provide the most sustainable foundation
for life and technology. Whenever he can, he likes to build something on his own
(than buy) and has a obsessive need to figure things out.

He is the Founder and hacker-in-charge of a Free Software Business called
DeepRoot Linux which he has lead since 2000. Most recently, he has built a new
one person company called Mostly Harmless to experiment with creative solutions
to problems that are generally thought to be someone else's.

## License

This lab manual is **(C) Copyright 2017, Abhas Abhinav and Mostly Harmless (OPC) Pvt. Ltd.**

Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU Free Documentation License, Version 1.3 or any later version
published by the Free Software Foundation; with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in
the section entitled "GNU Free Documentation License".

Source code, source code fragments, drawing and circuit diagrams included in
this manual are separately licensed under the GNU General Public License. You
can use, reditribute and/or modify the software under the terms of the GNU
General Public License as published by the Free Software Foundation, either
version 3 of the License or (at your option) any later version.

This manual and the contained software is distributed in the hope that it will
be useful but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
License for more details.

